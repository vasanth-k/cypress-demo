<body>
    <p>Hi Team,</p>
    <p>The <em>Cypress Automation Test</em>&nbsp;for the <strong>${group_name} Plan</strong> of the ST Fenix has been completed successfully.</p>
    <table style="width: 60%; margin-right: calc(50%);">
        <tbody>
            <tr>
                <td style="width: 45%;"><strong>Build Number</strong></td>
                <td style="width: 55%;"><a href="${buildURL}"
                        rel="noopener noreferrer" target="_blank">#${buildNumber}</a></td>
            </tr>
            <tr>
                <td style="width: 45%;"><strong>Environment</strong></td>
                <td style="width: 55%;">${environment}</td>
            </tr>
            <tr>
                <td style="width: 45%;"><strong>Test Execution Status</strong></td>
                <td style="width: 55%;">${buildStatus}</td>
            </tr>
            <tr>
                <td style="width: 45%;"><strong>Test Execution Report</strong></td>
                <td style="width: 55%;"><a href="${jenkinsReport}"
                        rel="noopener noreferrer" target="_blank">Click here</a></td>
            </tr>
        </tbody>
    </table>
    <p>For more details, please find the test execution log as attachment. Also you can download the complete test
        report <a href="${downloadUrl}"
            rel="noopener noreferrer" target="_blank">here</a>.</p>
    <p>Thank You.</p>
</body>

</html>