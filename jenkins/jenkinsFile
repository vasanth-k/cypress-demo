#!/usr/bin/env groovy

def emailTemplate(params) {
  env.WORKSPACE = pwd()
  def fileContents = readFile "${env.WORKSPACE}/jenkins/email.html.groovy"
  for (def data : params) {
    fileContents = fileContents.replace("\${" + data.getKey() + "}", data.getValue().toString())
  }
  return fileContents
}
pipeline {
  agent any
  environment {
    HOME = "${env.WORKSPACE}"
  }
  tools {
    nodejs 'nodejs 12.18.1'
  }
  parameters {
    choice(name: 'environment', choices: ['qa2', 'qa1', 'qa3'], description: 'This parameter represents the environment in which the test should run')
    string(name: 'group_name', defaultValue: 'High Priority Test', description: 'This parameter represents the name of the test plan that is to be executed')
    string(name: 'report_email', defaultValue: 'vasanth0591@gmail.com', description: 'This is the email id to which the report will be shared')
    choice(name: 'branch', choices: ['master'], description: 'This is the branch from which the test script will checkout')
  }
  stages {
    stage('Checkout Fenix UI Regression Test Repository') {
      steps {
        checkout([
          $class: 'GitSCM', 
          branches: [[name: '*/$branch']], 
          doGenerateSubmoduleConfigurations: false, 
          extensions: [
            [$class: 'RelativeTargetDirectory', relativeTargetDir: 'cypress-test'], 
            [$class: 'CleanBeforeCheckout'], 
            [$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: true]
          ], 
          submoduleCfg: [], 
          userRemoteConfigs: [[
            credentialsId: '8db4d00b-11fd-4e2a-9ccb-f83a8c2c2f6e', 
            url: 'https://vasanth-k@bitbucket.org/vasanth-k/cypress-demo.git'
          ]]
        ])
      }
    }

    stage('build') {
      steps {
        echo "Running build ${env.BUILD_ID} on ${env.JENKINS_URL}"
        bat 'cd cypress-test'
        bat 'npm ci'
        bat 'npm run cy:verify'
      }
    }

    stage('Cleanup Report') {
      steps {
        bat 'rmdir /s /q "cypress/reports"'
        bat 'mkdir "cypress/reports/html"'
      }
    }
    stage('Run Tests') {
      environment {
        // we will be recording test results and video on Cypress dashboard
        // to record we need to set an environment variable
        // we can load the record key variable from credentials store
        // see https://jenkins.io/doc/book/using/using-credentials/
        //CYPRESS_RECORD_KEY = credentials('cypress-example-kitchensink-record-key')
        // because parallel steps share the workspace they might race to delete
        // screenshots and videos folders. Tell Cypress not to delete these folders

        CYPRESS_RECORD_KEY = "07f53513-058a-4c15-b989-b5f32a466619"
        CYPRESS_trashAssetsBeforeRuns = 'false'
      }
      parallel {
        // start several test jobs in parallel, and they all
        // will use Cypress Dashboard to load balance any found spec files
        stage('Machine A') {
          steps {
            catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
              echo "Running build ${env.BUILD_ID}"
              bat "npm run cy:run"
            }
          }
        }
      }
    }
    stage('Generate Report') {
      steps {
        // sh "cd fenix-ui-reg-test-cypress; npx mochawesome-report-generator --inline true cypress/reports/mocha/mochawesome.json"
        bat "npm run combine-reports"
        bat "npm run generate-report"
      }
    }
  }
  post {
    always {
      publishHTML(target: [
        allowMissing: false, 
        alwaysLinkToLastBuild: false, 
        keepAll: true, 
        reportDir: 'cypress/reports/html', 
        reportFiles: 'report.html', 
        reportName: "Report", 
        reportTitles: 'Report'
      ])
      emailext attachLog: true,
      attachmentsPattern: "${env.BUILD_ID}Report/*zip*/Report.zip",
      mimeType: 'text/html',
      body: emailTemplate([
        "environment"   :   params.environment,
        "jenkinsReport" :   "${env.BUILD_URL}Report/",
        "buildStatus"   :   currentBuild.currentResult,
        "buildURL"      :   env.BUILD_URL,
        "buildNumber"   :   env.BUILD_NUMBER,
        "downloadUrl"   :   "${env.BUILD_URL}Report/*zip*/Report.zip",
        "group_name"    :   params.group_name
      ]),
      subject: "[" + env.JOB_NAME + "] [" + params.environment + "] - " + currentBuild.currentResult,
      from: 'vasanth0591@gmail.com',
      to: "${params.report_email}"
    }
  }
}
